## Esercizio 1
---
Creare la classe cerchio.
Il suo costruttore accetta un raggio(double) e la classe ha 3 metodi:
- calcolaDiametro -> restituisce il diametro
- calcolaCirconferenza -> restituisce la circonferenza
- calcolaArea -> indovina un po'.

Scrivere un programma che chiede all'utente  un raggio in ingresso e stampa a video diametro, circonferenza e area del cerchio.




