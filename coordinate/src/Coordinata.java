public class Coordinata {
    private double longitudine;
    private double latitudine;
    private int altitudine;

    public Coordinata(double longitudine, double latitudine, int altitudine) throws CoordinataErrata {

        if (longitudine < -180 || longitudine > 180 ||
            latitudine < -90 || latitudine > 90 ||
            altitudine < 0)
            throw new CoordinataErrata("La coordinata contiene valori non accettabili");

        this.longitudine = longitudine;
        this.latitudine = latitudine;
        this.altitudine = altitudine;
    }

    public double getLongitudine(){
        return longitudine;
    }

    public int getAltitudine() {
        return altitudine;
    }


    public double getLatitudine() {
        return latitudine;
    }
}
