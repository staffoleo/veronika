public class CoordinataErrata extends Exception {
    public CoordinataErrata(String message){
        super(message);
    }
}
