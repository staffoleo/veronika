import java.util.List;

public class Territorio {

    private String name;
    private List<Coordinata> vertici;


    public Territorio(String name, Coordinata vertice1, Coordinata vertice2, Coordinata vertice3) {

        this.name = name;
        vertici.add(vertice1);
        vertici.add(vertice2);
        vertici.add(vertice3);
    }

    public String getName() {
        return name;
    }

    public void AggiungiVertice(Coordinata nuovoVertice){
        if(vertici.size() <= 20)
           vertici.add(nuovoVertice);
    }

    public int altezzaMedia() {

        int altitudineTotale = 0;
        for (Coordinata coordinata : vertici) {
            altitudineTotale += coordinata.getAltitudine();
        }
        return altitudineTotale/vertici.size();
    }

    public String toString() {
        String accumulate = "";
        for (Coordinata coordinata: vertici) {
            accumulate += "coordinata: [longitudine: "+ coordinata.getLongitudine() + ", latitudine: " + coordinata.getLatitudine() +", altitudine: " + coordinata.getAltitudine() + "]\n\r";
        }
        return accumulate;
    }
}

