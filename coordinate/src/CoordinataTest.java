import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CoordinataTest {

    @Test
    public void GetLongitudineWorks() throws CoordinataErrata {

        Coordinata coordinata = new Coordinata( -123.1, 54.0, 450);
        assertEquals(coordinata.getLongitudine(), -123.1);
    }

    @Test
    public void GetLatitudineWorks() throws CoordinataErrata {

        Coordinata coordinata = new Coordinata( -123.1, 54.0, 450);
        assertEquals(coordinata.getLatitudine(), 54.0);
    }

    @Test
    public void GetAltitudineWorks() throws CoordinataErrata {

        Coordinata coordinata = new Coordinata( -123.1, 54, 450);
        assertEquals(coordinata.getAltitudine(), 450);
    }

    @Test
    public void ThrowsExceptionWorks() throws CoordinataErrata {

        Exception exception = assertThrows(Exception.class, () -> {
            Coordinata coordinata = new Coordinata( -193.1, 95.0, -10);
        });

        assertEquals("La coordinata contiene valori non accettabili", exception.getMessage());

    }

}
