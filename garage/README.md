## Garage
---
Scrivi un’applicazione per la gestione di un garage pubblico.
Il garage ha al massimo 15 posti, ognuno dei quali è identificato da un numero (a partire da zero) e, per motivi di capienza, può ospitare soltanto automobili, furgoni e motociclette.
Partendo dalla classe base VeicoloAMotore, la si estenda realizzando anche le classi che modellano le entità furgone, automobile e motocicletta. Ridefinire in particolare il metodo toString in modo che ogni entità possa esternalizzare in forma di stringa tutte le informazioni che la riguardano.
Poi implementa una classe che modelli il garage sopra descritto, offrendo le seguenti operazioni di gestione:
• Immissione di un nuovo veicolo nel garage (ritornare il numero del posto assegnato);
• Estrazione dal garage del veicolo che occupa un determinato posto (ritornare l’istanza del veicolo stesso);
• Stampa della situazione corrente dei posti nel garage: stampare per ogni posto tutte le informazioni disponibili sul veicolo che lo sta occupando.